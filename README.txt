RESULTCACHE MODULE

The result cache speeds up slow operations by storing function results in a
cache table and later, on page execution time, pulling these results from the 
cache table rather than calculating it.

Usage:

Where you before called:

$output = myfunc('do',
                 'something',
                 array('really' => 'slow'
                 );

call now:

$output = resultcache_getresult('myfunc',
                                'do',
                                'something',
                                array('really' => 'slow'
                                );
                                
alex [ at ] developmentseed [ dot ] org